from pprint import pprint
import gspread
import httplib2
import apiclient.discovery
from oauth2client.service_account import ServiceAccountCredentials
from asyncio import streams
import requests
from pytube import YouTube
import os
import pymysql
from datetime import datetime
import pandas as pd
import csv
import time
from transliterate import translit
import urllib.parse

#=-------------------------------------------------------------------------------------------------------------------------

CREDENTIALS_FILE = 'creds.json'
spreadsheet_id = '1Tcvldu2g1vjF7_imVTllH67aQJ6D8TyHcFOCxXIgwQs'
API_SERVICE_NAME = 'sheets'

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    CREDENTIALS_FILE,
    ['https://www.googleapis.com/auth/spreadsheets',
     'https://www.googleapis.com/auth/drive'])
httpAuth = credentials.authorize(httplib2.Http())
service = apiclient.discovery.build('sheets', 'v4', http = httpAuth)


values = service.spreadsheets().values().get(
    spreadsheetId=spreadsheet_id,
    range='B2:B1000',
    majorDimension='ROWS'
).execute()
##print(values)




#=-------------------------------------------------------------------------------------------------------------------------


mydb = pymysql.connect(
  host="localhost",
  user="user",
  database="wp_db",
  passwd="pass"
)

con = mydb.cursor()

#=-------------------------------------------------------------------------------------------------------------------------
gsheets = service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
sheets = gsheets['sheets']

for sheet in sheets:
    if sheet['properties']['title'] != 'master':
        dataset = service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id,
            range=sheet['properties']['title'],
            majorDimension='ROWS'
        ).execute()
        df = pd.DataFrame(dataset['values'])
        df.columns = df.iloc[0]
        df.drop(df.index[0], inplace=True)
        df.to_csv(sheet['properties']['title'] + '.csv', index=False)




#=-------------------------------------------------------------------------------------------------------------------------

csv_data = csv.reader(open('tabel.csv'))
header = next(csv_data)
for row in csv_data:
    number = row[0]
    url_video = row[1]
    name_video = row[2]
    description = row[3]
    description_2 =row[4]
    status = row[5]



    if status == 'download':

        sql_sel = "insert into video_csv(number,\
                                         url_video,\
                                         name_video,\
                                         description, \
                                         description_2,\
                                         status) \
                                         values \
                                         (%s,  %s, %s, %s, %s, %s)"

        stovp = (number, url_video, name_video, description, description_2, status)     

        con.execute(sql_sel, stovp)                          
        mydb.commit()

        time.sleep(-time.time()%2)
        time_server = datetime.now()

#=-------------------------------------------------------------------------------------------------------------------------            
      # Оголошуєм переміння для таблиці pavi_posts



        name_video_dov  = name_video

     #   name_en = translit(name_video_dov, language_code='ru', reversed=True)

        url_post = name_video_dov.replace(" ", "_")
        
        name_en = translit(name_video_dov, language_code='uk', reversed=True)
        
     
        url_post_1 = urllib.parse.quote(name_en)

        print (url_post_1)




        post_author = ("1")
        post_date = ("{0}".format(time_server))
        post_date_gmt = ("{0}".format(time_server))
        post_content = ("[video width=\"1280\" height=\"720\" mp4=\"https://intrgames.net/wp-content/uploads/2022/video/{0}.mp4\"][/video]".format(url_video))
        post_title = ("{0}".format(name_video))
        post_excerpt = ("{0}".format(description_2))
        post_status = ("publish")
        comment_status = ("closed")
        ping_status = ("closed")
        post_name = ("{0}".format(url_post_1))
        post_modified = ("{0}".format(time_server))
        post_modified_gmt = ("{0}".format(time_server))
        post_parent = ("0")
        guid = ("0808")
        menu_order = ("0")
        post_type = ("products")
        post_mime_type = ("")
        comment_count = ("0")        
      
#=-------------------------------------------------------------------------------------------------------------------------

        sql = "insert into pavi_posts(post_author, post_date, post_date_gmt, post_content, post_title, \
                                      post_excerpt, post_status, comment_status, ping_status, post_password, \
                                      post_name, to_ping, pinged, post_modified, post_modified_gmt, \
                                      post_content_filtered, post_parent, guid, menu_order, post_type, \
                                      post_mime_type, comment_count) \
               \
               \
               values(%s, %s, %s, %s, %s,\
                      %s, %s, %s, %s, %s,\
                      %s, %s, %s, %s, %s,\
                      %s, %s, %s, %s, %s,\
                      %s, %s)"
        
        



        perem = (post_author, post_date, post_date_gmt, post_content, post_title, \
                         post_excerpt, post_status, comment_status, ping_status, \
                         "", post_name, "", "", post_modified,\
                         post_modified_gmt, "", post_parent, guid, menu_order, \
                         post_type, post_mime_type, comment_count)

        con.execute(sql, perem)
        
        
        mydb.commit()  
#=-------------------------------------------------------------------------------------------------------------------------
        pav_posts_id = (con.lastrowid)
#
        guid_ubdata = ("https://intrgames.net/?post_type=products&#038;p={0}".format(pav_posts_id))
#
 #       sql_updata = "UPDATE pavi_posts SET post_content = '{1}' WHERE ID = '{0}' ".format(guid_ubdata, pav_posts_id)
 #       #val = ("done", "download")
 #       con.execute(sql_updata)
 #       mydb.commit()

        sql_updata = "UPDATE pavi_postmeta SET meta_key = %s WHERE meta_key = %s"

        val_updata = ("{0}", "0808".format(guid_ubdata))
        con.execute(sql_updata, val_updata)
        mydb.commit()


#=-------------------------------------------------------------------------------------------------------------------------


        meta_key_edit_last = ("_edit_last")
        meta_value_last = ("3054788")

        sql_edit_last = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_value_last= (pav_posts_id, meta_key_edit_last, meta_value_last)
 
        con.execute(sql_edit_last, id_value_last)
        mydb.commit()  
#=-------------------------------------------------------------------------------------------------------------------------

        meta_key_price_products = ("price-products")
        meta_value_price_products= ("125")

        sql_price_products = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_price_products= (pav_posts_id, meta_key_price_products, meta_value_price_products)
 
        con.execute(sql_price_products, id_price_products)
        mydb.commit()  

#=-------------------------------------------------------------------------------------------------------------------------
 
        meta_key_download = ("download")
        meta_value_download= ("{0}".format(url_video))

        sql_price_download = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_price_download = (pav_posts_id, meta_key_download, meta_value_download)
 
        con.execute(sql_price_download, id_price_download)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 
 
        meta_key_download2 = ("_download")
        meta_value_download2 = ("field_628f9100609de")

        sql_price_download2 = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_price_download2 = (pav_posts_id, meta_key_download2, meta_value_download2)
 
        con.execute(sql_price_download2, id_price_download2)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        meta_key_new_expert = ("new_expert")
        meta_value_new_expert = ("{0}".format(description))

        sql_price_new_expert = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_price_dnew_expert = (pav_posts_id, meta_key_new_expert, meta_value_new_expert)
 
        con.execute(sql_price_new_expert, id_price_dnew_expert)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        meta_key_new_expert2 = ("_new_expert")
        meta_value_new_expert2 = ("field_618a39e187778")

        sql_price_new_expert2 = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_price_dnew_expert2 = (pav_posts_id, meta_key_new_expert2, meta_value_new_expert2)
 
        con.execute(sql_price_new_expert2, id_price_dnew_expert2)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        meta_key_wp_trash_meta_status = ("_wp_trash_meta_status")
        meta_value_wp_trash_meta_status = ("private")

        sql_wp_trash_meta_status = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_wp_trash_meta_status = (pav_posts_id, meta_key_wp_trash_meta_status, meta_value_wp_trash_meta_status)
 
        con.execute(sql_wp_trash_meta_status, id_wp_trash_meta_status)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        meta_key_wp_trash_meta_time = ("_wp_trash_meta_time")
        meta_value_wp_trash_meta_time = ("1654102832")

        sql_wp_trash_meta_time = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_wp_trash_meta_time = (pav_posts_id, meta_key_wp_trash_meta_time, meta_value_wp_trash_meta_time)
 
        con.execute(sql_wp_trash_meta_time, id_wp_trash_meta_time)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        meta_key_wp_desired_post_slug = ("_wp_desired_post_slug")
        meta_value_wp_desired_post_slug = ("{0}".format(name_video))

        sql_wp_desired_post_slug = "insert into pavi_postmeta(post_id, meta_key, meta_value) values (%s, %s, %s)"
        id_wp_desired_post_slug = (pav_posts_id, meta_key_wp_desired_post_slug, meta_value_wp_desired_post_slug)
 
        con.execute(sql_wp_desired_post_slug, id_wp_desired_post_slug)
        mydb.commit()  

#=------------------------------------------------------------------------------------------------------------------------- 

        sql = "UPDATE video_csv SET status = %s WHERE status = %s"
        val = ("done", "download")
        con.execute(sql, val)
        mydb.commit()

#=------------------------------------------------------------------------------------------------------------------------- 


#service.spreadsheets().values().clear(

#        spreadsheetId=spreadsheet_id,
#        range='wp!A2:F1000'
       
#).execute()



#os.system("python3 dow_vide_intrgame.py")
#os.system("python3 dow_imgs_intrgame.py")
os.system("mkdir fails/{0}".format(time_server))
os.system("cp wp.csv fails/{0}".format(time_server))
